using System.Collections.Generic;
using Autowired.Core;
using Loogn.OrmLite;
using project.dao.Models;

namespace project.dao
{
    [AppService]
    public class SysRole_ResDao : BaseDao<SysRole_Res>
    {
    }

    [AppService]
    public class SysUser_ResDao : BaseDao<SysUser_Res>
    {
    }

    [AppService]
    public class SysUser_RoleDao : BaseDao<SysUser_Role>
    {
    }

    [AppService]
    public class SysResDao : BaseDao<SysRes>
    {
    }

    [AppService]
    public class SysUserDao : BaseDao<SysUser>
    {
        public List<SysUser_Res> GetUserResources(long sysUserId)
        {
            var sql = @"
 SELECT SysResId FROM SysUser_Res WHERE SysUserId={0}
 UNION  
 SELECT SysResId FROM SysRole_Res WHERE SysRoleId IN ( SELECT SysRoleId FROM SysUser_Role WHERE SysUserId={0}) ";

            using (var db = Open())
            {
                return db.SelectFmt<SysUser_Res>(sql, sysUserId);
            }
        }
    }

    [AppService]
    public class SysRoleDao : BaseDao<SysRole>
    {
    }
}