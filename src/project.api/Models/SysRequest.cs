using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata;
using project.dao.Models;

namespace project.api.Models
{
    #region 登录

    public class SysLoginRequest
    {
        [Required(ErrorMessage = "账号不能为空")] public string Account { get; set; }
        [Required(ErrorMessage = "密码不能为空")] public string Password { get; set; }
    }

    public class SysLoginResponse
    {
        public SysUserInfo UserInfo { get; set; }

        public string Jwt { get; set; }
    }

    #endregion

    #region 编辑用户

    public class SysEditUserRequest : LoginedRequest
    {
        public long Id { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 1-男，2-女
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// 1-正常,2-禁用
        /// </summary>
        public int Status { get; set; }

        //
        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }
    }

    #endregion

    #region 重置密码

    public class SysResetPasswordRequest
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassword { get; set; }
    }

    #endregion

    #region 用户列表

    public class SysUserListRequest : PagedRequest
    {
        public string Keyword { get; set; }
    }

    public class SysUserListResponse
    {
        public List<SysUser> List { get; set; }
        public long TotalCount { get; set; }
        public long PageCount { get; set; }
        public bool HasMore { get; set; }
    }

    #endregion

    #region 修改密码

    public class SysUpdatePasswordRequest : LoginedRequest
    {
        [Required(ErrorMessage = "旧密码不能为空")] public string OldPassword { get; set; }

        [Required(ErrorMessage = "新密码不能为空")] public string NewPassword { get; set; }

        [Required(ErrorMessage = "确认密码不能为空")]
        [Compare("NewPassword", ErrorMessage = "两次密码不正确")]
        public string RePassword { get; set; }
    }

    #endregion

    #region 加载菜单

    public class SysMenuDataRequest : LoginedRequest
    {
    }

    public class SysMenuDataResponse
    {
        public List<SysResInfo> Menus { get; set; }
    }

    #endregion


    #region 编辑资源

    public class SysEditResRequest : LoginedRequest
    {
        public long Id { get; set; }

        /// <summary>
        /// 资源名称
        /// </summary>
        [Required(ErrorMessage = "名称不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 资源地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 连接目标
        /// </summary>
        public string Target { get; set; }

        /// <summary>
        /// 资源类型，1-菜单，2-接口
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 1启用，2禁用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int OrderNum { get; set; }

        /// <summary>
        /// 父级菜单编号
        /// </summary>
        public long ParentId { get; set; }

        public List<SysResInfo> Children { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
    }

    #endregion

    public class SysResInfo
    {
        public long Id { get; set; }

        /// <summary>
        /// 资源名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 资源地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 连接目标
        /// </summary>
        public string Target { get; set; }

        // /// <summary>
        // /// 资源类型，1-菜单，2-接口
        // /// </summary>
        // public int Type { get; set; }
        //
        // /// <summary>
        // /// 1启用，2禁用
        // /// </summary>
        // public int Status { get; set; }
        //
        // /// <summary>
        // /// 排序号
        // /// </summary>
        // public int OrderNum { get; set; }

        /// <summary>
        /// 父级菜单编号
        /// </summary>
        public long ParentId { get; set; }

        public List<SysResInfo> Children { get; set; }
        // /// <summary>
        // /// 备注
        // /// </summary>
        // public string Remark { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
    }


    public class SysUserInfo
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 1-男，2-女
        /// </summary>
        public int Gender { get; set; }
    }


    public class SysIdRequest : LoginedRequest
    {
        /// <summary>
        /// 要删除的编号
        /// </summary>
        public long Id { get; set; }
    }
}