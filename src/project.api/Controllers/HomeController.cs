﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autowired.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PagedList.Core;

namespace project.api.Controllers
{
    public class HomeController : Controller
    {
        [Autowired] private IWebHostEnvironment env;
        [Autowired] private ILogger<HomeController> logger;

        public HomeController(AutowiredService autowiredService)
        {
            autowiredService.Autowired(this);
        }

        public IActionResult Index(int page = 1)
        {
            logger.LogInformation("请求了首页：" + DateTime.Now);

            var listdata = new List<int>();
            for (var i = 0; i < 100; i++)
            {
                listdata.Add(i);
            }

            var pageSize = 10;
            var pagedList = new StaticPagedList<int>(listdata.Skip(pageSize * (page - 1)).Take(pageSize), page,
                pageSize, listdata.Count);
            ViewBag.pagedList = pagedList;

            var claimList = HttpContext.User.Claims.Select(x => x.Type + ":" + x.Value).ToList();
            ViewBag.claimList = claimList;

            ViewBag.Environment = env.EnvironmentName;
            return View();
        }
    }
}